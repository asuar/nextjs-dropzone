/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['firebasestorage.googleapis.com'],
  },
  assetPrefix:
    process.env.NODE_ENV === 'production' ? '/nextjs-dropzone' : './',
};

module.exports = nextConfig;
