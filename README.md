# Nextjs Dropzone

[View live here!](https://asuar.gitlab.io/nextjs-dropzone/)

A Nextjs application for uploading images to a Firebase storage bucket. It uses the Mantine library for drag and drop functionality.

## Features :star2:

:star: Drag and Drop files\
:star: Upload files\
:star: Image preview\
:star: Size restrictions\
:star: File type validation

## Technologies used 🛠️

- [Next.js](https://nextjs.org) - React framework for building web applications
- [React](https://es.reactjs.org/) - Front-End JavaScript library
- [Mantine](https://mantine.dev/) - React components and hooks library
- [Firebase](https://firebase.google.com/) - Cloud storage
- [React Icons](https://react-icons.github.io/react-icons) - React Library for icons
- [Moment](https://momentjs.com/) - JavaScript date library for parsing, validating, manipulating, and formatting dates.

## License 📄

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
