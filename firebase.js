// Import the functions you need from the SDKs you need
import { initializeApp, getApps, getApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyC9iLE-j5AGzC9kUpJINB3Rnjj6ov0eoDA',
  authDomain: 'nextjs-dropzone.firebaseapp.com',
  projectId: 'nextjs-dropzone',
  storageBucket: 'nextjs-dropzone.appspot.com',
  messagingSenderId: '808951382110',
  appId: '1:808951382110:web:0c6d84de9e2983212c34cf',
};

// Initialize Firebase
const app = getApps().length === 0 ? initializeApp(firebaseConfig) : getApp();
const database = getFirestore();
const storage = getStorage();
export { app, database, storage };
