import { useState } from 'react';
import {
  Group,
  Text,
  useMantineTheme,
  Image,
  SimpleGrid,
  Button,
} from '@mantine/core';
import {
  Dropzone as MantineDropzone,
  DropzoneProps as MantineDropzoneProps,
  IMAGE_MIME_TYPE,
} from '@mantine/dropzone';
import { BiUpload, BiImageAdd, BiBlock } from 'react-icons/bi';
import { database, storage } from '../../firebase';
import { ref, getDownloadURL, uploadBytes } from '@firebase/storage';
import {
  addDoc,
  arrayUnion,
  updateDoc,
  collection,
  serverTimestamp,
  doc,
} from 'firebase/firestore';

export const Dropzone = (props) => {
  const [files, setFiles] = useState([]);
  const theme = useMantineTheme();

  const uploadFile = async () => {
    const docRef = await addDoc(collection(database, 'images'), {
      images: [],
      timestamp: serverTimestamp(),
    });

    await Promise.all(
      files.map((file) => {
        const fileRef = ref(storage, `images/${docRef.id}/${file.path}`);
        uploadBytes(fileRef, file)
          .then(async () => {
            const downloadURL = await getDownloadURL(fileRef);
            await updateDoc(doc(database, 'images', docRef.id), {
              images: arrayUnion(downloadURL),
            });
          })
          .catch((err) => alert(err));
      })
    );
    setFiles([]);
  };

  const previews = files.map((file, index) => {
    const imageUrl = URL.createObjectURL(file);
    return (
      <Image
        key={index}
        src={imageUrl}
        alt={file.name}
        imageProps={{ onLoad: () => URL.revokeObjectURL(imageUrl) }}
      />
    );
  });

  return (
    <>
      <MantineDropzone
        onDrop={setFiles}
        onReject={(files) => console.log('rejected', files)}
        maxSize={1024 * 1024 * 5} // 5mb
        accept={IMAGE_MIME_TYPE}
        {...props}
      >
        <Group
          position="center"
          spacing="xl"
          style={{ minHeight: 220, pointerEvents: 'none' }}
        >
          <MantineDropzone.Accept>
            <BiUpload
              size={50}
              color={
                theme.colors[theme.primaryColor][
                  theme.colorScheme === 'dark' ? 4 : 6
                ]
              }
            />
          </MantineDropzone.Accept>
          <MantineDropzone.Reject>
            <BiBlock
              size={50}
              color={theme.colors.red[theme.colorScheme === 'dark' ? 4 : 6]}
            />
          </MantineDropzone.Reject>
          <MantineDropzone.Idle>
            <BiImageAdd size={50} />
          </MantineDropzone.Idle>

          <div>
            <Text size="xl" inline>
              Drag files or click to upload
            </Text>
            <Text size="sm" color="dimmed" inline mt={7}>
              Each file should not exceed 5mb
            </Text>
          </div>
        </Group>
      </MantineDropzone>
      <Button
        mt="md"
        onClick={() => {
          files.length > 0 ? uploadFile() : alert('Must select a file.');
        }}
      >
        Submit
      </Button>
      <SimpleGrid
        cols={2}
        breakpoints={[{ maxWidth: 'sm', cols: 1 }]}
        mt={previews.length > 0 ? 'xl' : 0}
      >
        {previews}
      </SimpleGrid>
    </>
  );
};

export default Dropzone;
