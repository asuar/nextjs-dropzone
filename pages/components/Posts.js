import React, { useEffect, useState } from 'react';
import { collection, onSnapshot, orderBy, query } from 'firebase/firestore';
import { database } from '../../firebase';
import { Image } from '@mantine/core';
import moment from 'moment';

export const Posts = () => {
  const [posts, setPosts] = useState([]);

  const postImages = (post) => {
    const post_images = post.images?.map((file) => (
      <div key={`image-${file.id}`} className="image-container">
        <Image src={file} alt="post image" />
      </div>
    ));
    return post_images;
  };

  useEffect(() => {
    const collectionRef = collection(database, 'images');
    const q = query(collectionRef, orderBy('timestamp', 'desc'));
    const unsubscribe = onSnapshot(q, (snapshot) => {
      setPosts(
        snapshot.docs.map((doc) => ({
          ...doc.data(),
          id: doc.id,
          timestamp: doc.data().timestamp?.toDate().getTime(),
        }))
      );
    });

    return unsubscribe;
  }, []);

  return (
    <div>
      {posts.map((post) => (
        <div key={post.id} className="image-post-container">
          {postImages(post)}
          <p className="timestamp">{moment(post.timestamp).fromNow()}</p>
        </div>
      ))}
    </div>
  );
};

export default Posts;
