import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Dropzone from './components/dropzone';
import { MantineProvider, Grid, useMantineTheme } from '@mantine/core';
import { Posts } from './components/Posts';

export default function Home() {
  const theme = useMantineTheme();
  return (
    <div className={styles.container}>
      <Head>
        <title>Dropzone</title>
        <meta name="description" content="Dropzone app in Nextjs" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <MantineProvider withGlobalStyles withNormalizeCSS>
          <Grid columns={24} sx={{ minHeight: '100vh', margin: '0px' }}>
            <Grid.Col
              lg={6}
              xs={12}
              sx={{
                textAlign: 'center',
                background: 'white',
                padding: theme.spacing.xl,
              }}
            >
              <Dropzone />
            </Grid.Col>
            <Grid.Col span="auto" sx={{ background: '#cfcfcf' }}>
              <Posts />
            </Grid.Col>
          </Grid>
        </MantineProvider>
      </main>
    </div>
  );
}
